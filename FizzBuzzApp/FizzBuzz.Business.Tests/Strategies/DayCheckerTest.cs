﻿﻿namespace FizzBuzz.Business.Test.Strategies
{
    using FizzBuzz.Business.Strategies;
    using FluentAssertions;
    using NUnit.Framework;
    using System;

    internal class DayCheckerTest
    {
        private DayChecker dayChecker;

        [SetUp]
        public void Setup()
        {
            dayChecker = new DayChecker();
            dayChecker.SpecifiedDayOfWeek = DayOfWeek.Wednesday;
        }

        [TestCase(("09/09/2019"), false)]
        [TestCase(("09/10/2019"), false)]
        [TestCase(("09/11/2019"), true)]
        public void IsDayOfWeekReturnsValidOutputBasedOnDay(DateTime day, bool expectedOutput)
        {
            // Action
            bool actualOutput = dayChecker.IsSpecifiedDayOfWeek(day);

            // Assert
            //Assert.areEqual(expectedOutput,actualOutput);
            actualOutput.Should().Be(expectedOutput);
        }
    }
}