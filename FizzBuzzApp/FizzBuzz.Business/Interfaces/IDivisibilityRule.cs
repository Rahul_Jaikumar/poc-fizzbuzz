﻿namespace FizzBuzz.Business.Interfaces
{
    public interface IDivisibilityRule
    {
        int Order { get; set; }

        bool IsDivisibleBy(int number);

        string GetFizzBuzzValue();
    }
}