﻿namespace FizzBuzz.Business.Interfaces
{
    using System.Collections.Generic;

    public interface IFizzBuzzBusiness
    {
        IList<string> GetFizzBuzzList(int number);
    }
}