﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1101:Prefix local calls with this", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzz.Business.Strategies.DivisibileByFive.#ctor(FizzBuzz.Business.Interfaces.IDayCheckerRule)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1101:Prefix local calls with this", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzz.Business.Strategies.DivisibileByFive.GetFizzBuzzValue~System.String")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1101:Prefix local calls with this", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzz.Business.Strategies.DivisibileByThree.#ctor(FizzBuzz.Business.Interfaces.IDayCheckerRule)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1101:Prefix local calls with this", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzz.Business.Strategies.DivisibileByThree.GetFizzBuzzValue~System.String")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1101:Prefix local calls with this", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzz.Business.Strategies.DivisibileByThreeAndFive.#ctor(FizzBuzz.Business.Interfaces.IDayCheckerRule)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1101:Prefix local calls with this", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzz.Business.Strategies.DivisibileByThreeAndFive.GetFizzBuzzValue~System.String")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1101:Prefix local calls with this", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzz.Business.Strategies.FizzBuzzBusiness.#ctor(System.Collections.Generic.IEnumerable{FizzBuzz.Business.Interfaces.IDivisibilityRule})")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1101:Prefix local calls with this", Justification = "<Pending>", Scope = "member", Target = "~M:FizzBuzz.Business.Strategies.FizzBuzzBusiness.GetFizzBuzzList(System.Int32)~System.Collections.Generic.IList{System.String}")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1633:The file header is missing or not located at the top of the file.", Justification = "<Pending>", Scope = "namespace", Target = "~N:FizzBuzz.Business.Interfaces")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1633:The file header is missing or not located at the top of the file.", Justification = "<Pending>", Scope = "namespace", Target = "~N:FizzBuzz.Business.Strategies")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1208:Using directive for 'System' should appear before directive for 'FizzBuzz.Business.Interfaces'", Justification = "<Pending>", Scope = "namespace", Target = "~N:FizzBuzz.Business.Strategies")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Build", "SA1649:File name should match first type name.", Justification = "<Pending>", Scope = "type", Target = "~T:FizzBuzz.Business.Strategies.DivisibileByThree")]