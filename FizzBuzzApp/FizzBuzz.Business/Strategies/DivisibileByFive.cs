﻿﻿namespace FizzBuzz.Business.Strategies
{
    using FizzBuzz.Business.Interfaces;
    using System;
    //using FizzBuzz.Business.Strategies;

    public class DivisibileByFive : IDivisibilityRule
    {
        private readonly IDayCheckerRule dayCheckerRule;

        public DivisibileByFive(IDayCheckerRule dayChecker)
        {
            //dayCheckerRule = new DayChecker();
            dayCheckerRule = dayChecker;
        }

        public int Order { get; set; }

        public bool IsDivisibleBy(int number)
        {
            return number % 5 == 0;
        }

        public string GetFizzBuzzValue()
        {
            return dayCheckerRule.IsSpecifiedDayOfWeek(DateTime.Now) ? "Wuzz" : "Buzz";
        }
    }
}
