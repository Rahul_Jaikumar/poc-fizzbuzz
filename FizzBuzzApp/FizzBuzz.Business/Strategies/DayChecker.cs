﻿﻿namespace FizzBuzz.Business.Strategies
{
    using System;
    using FizzBuzz.Business.Interfaces;

    public class DayChecker : IDayCheckerRule
    {
        public DayOfWeek SpecifiedDayOfWeek { get; set; }

        public bool IsSpecifiedDayOfWeek(DateTime date)
        {
            //return date.DayOfWeek == DayOfWeek.Wednesday;
            return date.DayOfWeek == this.SpecifiedDayOfWeek;
            
        }
    }
}